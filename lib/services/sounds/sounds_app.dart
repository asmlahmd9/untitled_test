import 'package:audioplayers/audioplayers.dart';

class SoundsApp {
  Future<void> sounding() async {
    AudioPlayer audioPlayer = AudioPlayer();
    int result = await audioPlayer.play(
        'https://freetestdata.com/wp-content/uploads/2021/09/Free_Test_Data_100KB_MP3.mp3');
    if (result == 1) {
      print('Audio played successfully');
    }
  }
}
