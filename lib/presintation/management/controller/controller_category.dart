import 'package:get/get.dart';
import 'package:untitled_test_ahmed/core/strings/messages.dart';
import 'package:untitled_test_ahmed/data/database/category_remote_data_source.dart';
import 'package:untitled_test_ahmed/data/database/create_database.dart';
import 'package:untitled_test_ahmed/data/models/category_model.dart';
import 'package:untitled_test_ahmed/data/models/favorite_Item.dart';

import 'package:untitled_test_ahmed/services/notifications/notifications_app.dart';
import 'package:untitled_test_ahmed/services/sounds/sounds_app.dart';

class CategoryController extends GetxController {
  final data = <Datum>[].obs;

  final dataLocale = <Datum>[].obs;
  var dataIsFavoriteSql = <Map<dynamic, dynamic>>[];
  final dataIsFavorite = <bool>[];
  bool isFavorite = false;

  @override
  void onInit() {
    super.onInit();
    getIsFavorite();
    fetchData();
  }

  bool searchList(dynamic searchValue) {
    for (final map in dataIsFavoriteSql) {
      if (map.containsValue(searchValue)) {
        return true;
      }
    }
    return false;
  }

  Future<void> getIsFavorite() async {
    dataIsFavoriteSql.assignAll(await SqlDb.read('favorite'));
  }

  void _updateFavorite(int index, int id, bool isFavorite) async {
    final favoriteItem = FavoriteItem(id: id, isFavorite: isFavorite);
    await SqlDb.insertOrUpdateFavorite(favoriteItem);
    data[index].isFavorite = isFavorite;
    NotificationsApp().initNotifications(
        title: isFavorite ? ADD_Favorite : REMOVE_Favorite,
        body: isFavorite ? ADD_Favorite_ITEM : REMOVE_Favorite_ITEM);
    SoundsApp().sounding();
    update();
  }

  void addFavorite({required int index, required int id}) =>
      _updateFavorite(index, id, true);

  void removeFavorite({required int index, required int id}) =>
      _updateFavorite(index, id, false);

  Future<void> fetchData() async =>
      data.assignAll(await CategoryRemoteDataSource.fetchData());
}
