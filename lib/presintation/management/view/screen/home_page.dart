import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:untitled_test_ahmed/presintation/management/view/widget/build_category.dart';

import '../../controller/controller_category.dart';

class HomePage extends StatelessWidget {
  final CategoryController controller = Get.put(CategoryController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Category'),
      ),
      body: Obx(() {
        if (controller.data.isEmpty) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else {
          return ListView.builder(
            itemCount: controller.data.length,
            itemBuilder: (context, index) {
              final category = controller.data[index];
              category.isFavorite =
                  controller.searchList(int.parse(category.id));
              return BuildCategory(
                category: category,
                controller: controller,
                index: index,
              );
            },
          );
        }
      }),
    );
  }
}
