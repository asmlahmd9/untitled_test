import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:untitled_test_ahmed/data/models/category_model.dart';
import 'package:untitled_test_ahmed/presintation/management/view/screen/view_map.dart';
import 'package:untitled_test_ahmed/presintation/management/view/widget/components.dart';

class ViewCategory extends StatelessWidget {
  final Datum category;

  const ViewCategory({Key? key, required this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
        Card(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
                child: Column(children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Text('total : ${category.address}'),
                  const SizedBox(
                    height: 20,
                  ),
                  Text('created at : ${category.createdAt.toIso8601String()}'),
                  const SizedBox(
                    height: 30,
                  ),
                  IconButton(
                      onPressed: () {
                        navigateTo(
                            context,
                            ViewMap(
                              address:category.address ,
                            ));
                      },
                      icon: const Icon(
                        Icons.map,size: 30,
                        color: Colors.indigo,
                      ))
                ]),
              ),
            ],
          ),
        ),
        SizedBox(
          child: GridView.count(
            shrinkWrap: true,
            crossAxisCount: 2,
            children: List.generate(category.items.length, (index) {
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 6),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.indigo.shade200,
                      borderRadius: BorderRadius.circular(30)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          category.items[index].name,
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'price ${category.items[index].price}',
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }),
          ),
        ),
      ]),
    );
  }
}
