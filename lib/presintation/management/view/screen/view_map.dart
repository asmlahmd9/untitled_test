import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:untitled_test_ahmed/data/models/category_model.dart';

class ViewMap extends StatefulWidget {
  final Address address;

  const ViewMap({Key? key, required this.address}) : super(key: key);

  @override
  _ViewMapState createState() => _ViewMapState();
}

class _ViewMapState extends State<ViewMap> {
  late GoogleMapController mapController;
  final Set<Marker> _markers = {};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Location address on the map'),
      ),
      body: GoogleMap(
        onMapCreated: (GoogleMapController controller) {
          setState(() {
            mapController = controller;
            _markers.add(
              Marker(
                markerId: const MarkerId('destination'),
                position: LatLng(double.parse(widget.address.lat),
                    double.parse(widget.address.lng)),
              ),
            );
          });
        },
        initialCameraPosition: CameraPosition(
          target: LatLng(double.parse(widget.address.lat),
              double.parse(widget.address.lng)),
          zoom: 15,
        ),
        markers: _markers,
      ),
    );
  }
}
