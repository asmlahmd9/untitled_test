import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:untitled_test_ahmed/data/models/category_model.dart';
import 'package:untitled_test_ahmed/domain/usecases/favorite.dart';
import 'package:untitled_test_ahmed/presintation/management/controller/controller_category.dart';
import 'package:untitled_test_ahmed/presintation/management/view/screen/view_category.dart';
import 'package:untitled_test_ahmed/presintation/management/view/widget/components.dart';

class BuildCategory extends StatelessWidget {
  final Datum category;
  final CategoryController controller;
  final int index;
  const BuildCategory({Key? key, required this.category, required this.controller, required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        navigateTo(
            context,
            ViewCategory(
              category: category,
            ));
      },
      child: Card(
        child: Padding(
          padding:
          const EdgeInsets.symmetric(horizontal: 4, vertical: 10),
          child: Row(
            children: [
              SizedBox(
                  width: 100,
                  height: 100,
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(50),
                      child: Image.network(
                        category.image,
                        fit: BoxFit.cover,
                      ))),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 20,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Total :${category.total}'),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      '${category.createdAt.year} - ${category.createdAt.day} - ${category.createdAt.month}',
                    ),
                    Text(category.currency.name),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                ),
                child: GetBuilder<CategoryController>(
                  builder: (GetxController _controller) {
                    return IconButton(
                        color: Colors.red,
                        onPressed: () {
                          if (category.isFavorite) {


                            controller.removeFavorite(
                                index: index,
                                id: int.parse(category.id));
                            category.isFavorite = false;
                          } else {
                            controller.addFavorite(
                                index: index,
                                id: int.parse(category.id));
                            category.isFavorite = true;
                          }
                          print(category.isFavorite);
                        },
                        icon: category.isFavorite
                            ? const Icon(Icons.favorite)
                            : const Icon(Icons.favorite_border));
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}