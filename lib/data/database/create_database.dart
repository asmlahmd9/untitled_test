import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import '../models/favorite_Item.dart';
import 'create_tables.dart';

class SqlDb {
  static Database? _db;

  static Future<Database?> get db async {
    if (_db == null) {
      _db = await initDb();
      return _db;
    } else {
      return _db;
    }
  }

  static initDb() async {
    String databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'test_a.db');
    Database mydb = await openDatabase(path,
        version: 1, onCreate: onCreate, onUpgrade: onUpgrade);
    return mydb;
  }

  static onUpgrade(Database db, int oldVersion, int newVersion) async {
    await db.execute("ALTER TABLE test_a ADD COLUMN color TEXT");
    print(
        'onUpgrade ==============================================================');
  }



  static read(String table) async {
    Database? mydb = await db;
    List<Map> response = await mydb!.query(table);
    print(
        'read ==============================================================');
    return response;
  }

  static insert(String table, Map<String, Object?> values) async {
    Database? mydb = await db;
    int response = await mydb!.insert(table, values);
    print(
        'insert ==============================================================');
    print(response);
    return response;
  }

  static update(
      String table, Map<String, Object?> values, String myWhere) async {
    Database? mydb = await db;
    int response = await mydb!.update(table, values, where: myWhere);
    print(
        'update ==============================================================');
    return response;
  }

  static delete(String table, String myWhere) async {
    Database? mydb = await db;
    int response = await mydb!.delete(table, where: myWhere);
    print(
        'delete ==============================================================');
    return response;
  }


  static Future<void> insertOrUpdateFavorite(FavoriteItem favoriteItem) async {
    final mydb = await db;
    await mydb!.insert(
      'favorite',
      favoriteItem.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  static Future<bool> isFavorite(int id) async {
    final mydb = await db;
    final result = await mydb!.query(
      'favorite',
      where: 'is_favorite = ?',
      whereArgs: [id],
    );
    if (result.isNotEmpty) {
      final row = result.first;
      return row['is_favorite'] == 1;
    }
    return false;
  }


  static myDeleteDatabase() async {
    String databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'test_a.db');
    await deleteDatabase(path);
  }
}
