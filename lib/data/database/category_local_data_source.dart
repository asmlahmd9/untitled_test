import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:untitled_test_ahmed/data/models/category_model.dart';

const CACHED_Category = 'CACHED_Category';

late SharedPreferences sharedPreferences;

class CategoryLocalDataSource {
  @override
  Future<Unit> cacheCategory(List<Datum> categoryModel) {
    List categoryModelToJson = categoryModel
        .map<Map<String, dynamic>>((categoryModel) => categoryModel.toJson())
        .toList();
    sharedPreferences.setString(
        CACHED_Category, jsonEncode(categoryModelToJson));
    return Future.value(unit);
  }

  @override
  Future<List<Datum>> getCachedCategory() {
    final jsonString = sharedPreferences.getString(CACHED_Category);
    if (jsonString != null) {
      List decodeJsonData = json.decode(jsonString);

      List<Datum> jsonToCategoryModel = decodeJsonData
          .map<Datum>((jsonCategoryModel) => Datum.fromJson(jsonCategoryModel))
          .toList();
      return Future.value(jsonToCategoryModel);
    } else {
      throw Exception('Failed');
    }
  }
}
