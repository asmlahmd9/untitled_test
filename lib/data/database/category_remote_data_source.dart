
import 'dart:convert';

import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:untitled_test_ahmed/core/strings/string_app.dart';
import 'package:untitled_test_ahmed/data/database/category_local_data_source.dart';
import 'package:untitled_test_ahmed/data/models/category_model.dart';
import 'package:http/http.dart' as http;

class CategoryRemoteDataSource {
  static Future<List<Datum>> fetchData() async {
    final result = await InternetConnectionChecker().hasConnection;
    try {
      if (result) {
        final response = await http.get(Uri.parse('$BASE_URL/orders'));
        if (response.statusCode == 200) {
          final jsonData = json.decode(response.body);
          final dataList = jsonData['data']
              .map<Datum>((data) => Datum.fromJson(data))
              .toList();
          CategoryLocalDataSource().cacheCategory(dataList);
          return dataList;
        } else {
          throw Exception('Failed to fetch data');
        }
      } else {
        final dataLocal =
        await CategoryLocalDataSource().getCachedCategory();
        return dataLocal;
      }
    } catch (e) {
      print('Error fetching data: $e');
      return [];
    }
  }
}