import 'package:sqflite/sqflite.dart';

void onCreate(Database db, int version) async {
  Batch batch = db.batch();

  batch.execute('''
CREATE TABLE "favorite" (
	"id"	INTEGER NOT NULL,
	"id_favorite"	INTEGER,
	"is_favorite"	BLOB,
	PRIMARY KEY("id" AUTOINCREMENT)
);
    ''');

  await batch.commit();
  print(
      'onCreate ==============================================================');
}
