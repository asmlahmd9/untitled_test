class FavoriteItem {
  final int id;
  final bool isFavorite;

  FavoriteItem({required this.id, required this.isFavorite});

  Map<String, dynamic> toMap() {
    return {
      'id_favorite': id,
      'is_favorite': isFavorite ? 1 : 0, // 1 = true, 0 = false
    };
  }

  factory FavoriteItem.fromJson(Map<String, dynamic> json) => FavoriteItem(
        id: json['id'],
        isFavorite: json['id_favorite'],
      );
}
