// To parse this JSON data, do
//
//     final categoryModel = categoryModelFromJson(jsonString);

import 'dart:convert';

CategoryModel categoryModelFromJson(String str) =>
    CategoryModel.fromJson(json.decode(str));

String categoryModelToJson(CategoryModel data) => json.encode(data.toJson());

class CategoryModel {
  CategoryModel({
    required this.data,
    required this.code,
    required this.message,
    required this.paginate,
  });

  List<Datum> data;
  int code;
  String message;
  Paginate paginate;

  factory CategoryModel.fromJson(Map<String, dynamic> json) => CategoryModel(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        code: json["code"],
        message: json["message"],
        paginate: Paginate.fromJson(json["paginate"]),
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "code": code,
        "message": message,
        "paginate": paginate.toJson(),
      };
}

class Datum {
  Datum({
    required this.total,
    required this.createdAt,
    required this.image,
    required this.currency,
    required this.id,
    required this.address,
    required this.items,
    this.isFavorite = false,
    this.page,
    this.limit,
  });

  bool isFavorite;
  String total;
  DateTime createdAt;
  String image;
  Currency currency;
  String id;
  Address address;
  List<Item> items;
  int? page;
  int? limit;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        total: json["total"],
        createdAt: DateTime.parse(json["created_at"]),
        image: json["image"],
        currency: currencyValues.map[json["currency"]]!,
        id: json["id"],
        address: Address.fromJson(json["address"]),
        items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
        page: json["page"],
        limit: json["limit"],
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "created_at": createdAt.toIso8601String(),
        "image": image,
        "currency": currencyValues.reverse[currency],
        "id": id,
        "address": address.toJson(),
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
        "page": page,
        "limit": limit,
      };
}

class Address {
  Address({
    required this.lat,
    required this.lng,
  });

  String lat;
  String lng;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        lat: json["lat"],
        lng: json["lng"],
      );

  Map<String, dynamic> toJson() => {
        "lat": lat,
        "lng": lng,
      };
}

enum Currency { LBP }

final currencyValues = EnumValues({"LBP": Currency.LBP});

class Item {
  Item({
    required this.id,
    required this.name,
    required this.price,
  });

  int id;
  String name;
  String price;

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        id: json["id"],
        name: json["name"],
        price: json["price"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "price": price,
      };
}

class Paginate {
  Paginate({
    required this.total,
    required this.perPage,
  });

  int total;
  int perPage;

  factory Paginate.fromJson(Map<String, dynamic> json) => Paginate(
        total: json["total"],
        perPage: json["per_page"],
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "per_page": perPage,
      };
}

class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap = map.map((k, v) => MapEntry(v, k));
    return reverseMap;
  }
}
