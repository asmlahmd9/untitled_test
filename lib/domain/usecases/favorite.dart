
import 'package:untitled_test_ahmed/core/strings/messages.dart';
import 'package:untitled_test_ahmed/data/database/create_database.dart';
import 'package:untitled_test_ahmed/data/models/favorite_Item.dart';
import 'package:untitled_test_ahmed/presintation/management/controller/controller_category.dart';
import 'package:untitled_test_ahmed/services/notifications/notifications_app.dart';
import 'package:untitled_test_ahmed/services/sounds/sounds_app.dart';

class FavoriteController {
  void removeFavorite({required int index, required int id}) async {
    final favoriteItem = FavoriteItem(id: id, isFavorite: false);
    await SqlDb.insertOrUpdateFavorite(favoriteItem);
    if (index >= 0 && index < CategoryController().data.length) {
      CategoryController().data[index].isFavorite = false;
      await SqlDb.delete('favorite', 'id_favorite=$id');
      NotificationsApp().initNotifications(
          title: REMOVE_Favorite, body: REMOVE_Favorite_ITEM);
      SoundsApp().sounding();
      CategoryController().update();
    }
  }

  void addFavorite({required int index, required int id}) async {
    final favoriteItem = FavoriteItem(id: id, isFavorite: true);
    await SqlDb.insertOrUpdateFavorite(favoriteItem);
    if (index >= 0 && index < CategoryController().data.length) {
      CategoryController().data[index].isFavorite = true;
      NotificationsApp()
          .initNotifications(title: ADD_Favorite, body: ADD_Favorite_ITEM);
      SoundsApp().sounding();
      CategoryController().update();
    }
  }
}
